'use strict';

const expect = require('chai').expect;
const Winston = require('winston');
const logger = require('../index');

describe('Require tests', function() {
  it('Should return an instance of Winston.Logger with default option (console enabled, loggly disabled)', () => {
    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.have.deep.property('transports.console')
                  .and.not.have.deep.property('transports.loggly');
    expect(logger.transports.console).to.be.an.instanceof(Winston.transports.Console);
  });

  it('Should configure the logger instance with loggly transport', () => {
    require('../index').setup({
      loggly: {
        enabled: true,
        options: {
          token: 'my-super-token',
          subdomain: 'foo'
        }
      }
    });

    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.have.deep.property('transports.loggly');
    expect(logger.transports.loggly).to.be.an.instanceof(Winston.transports.Loggly);
  });

  it('Should disable the console transport', () => {
    require('../index').setup({
      console: {
        enabled: false
      }
    });

    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.not.have.deep.property('transports.console');
  });

  it('Should disable the loggly transport', () => {
    require('../index').setup({
      loggly: {
        enabled: false
      }
    });

    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.not.have.deep.property('transports.loggly');
  });

  it('Should handle exceptions in console', () => {
    require('../index').setup({
      console: {
        enabled: true
      },
      handle_exceptions: true
    });

    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.have.deep.property('exceptionHandlers.console');
  });

  it('Should not handle any exceptions', () => {
    require('../index').setup({
      console: {
        enabled: true
      },
      handle_exceptions: false
    });

    expect(logger).to.be.an.instanceof(Winston.Logger)
                  .and.not.have.deep.property('exceptionHandlers.console');
  });

  it('Should throw an error in case of bad settings', () => {
    expect( () => {
      require('../index').setup({
        test: false
      });
    }).to.throw(Error);
  });
});
