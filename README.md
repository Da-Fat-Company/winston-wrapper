# Winston Wrapper

[![build status](https://gitlab.com/Da-Fat-Company/winston-wrapper/badges/master/build.svg)](https://gitlab.com/Da-Fat-Company/winston-wrapper/commits/master)
[![NPM Badge](https://img.shields.io/npm/v/@da-fat-company/winston-wrapper.svg)](https://www.npmjs.com/package/@da-fat-company/winston-wrapper)
[![coverage report](https://gitlab.com/Da-Fat-Company/winston-wrapper/badges/master/coverage.svg)](https://da-fat-company.gitlab.io/winston-wrapper/coverage)
[![JsDoc report](https://img.shields.io/badge/link-jsdoc-green.svg)](https://da-fat-company.gitlab.io/winston-wrapper/jsdoc)
[![Plato report](https://img.shields.io/badge/link-plato-green.svg)](https://da-fat-company.gitlab.io/winston-wrapper/plato)

A simple WinstonJS Wrapper to centralize logging transports.

## Install

```bash
    $ npm i --save @da-fat-company/winston-wrapper
```

## Use

```javascript
'use strict';

const logger = require('@da-fat-company/winston-wrapper');

logger.log('Hello World');
```

You can also configure the logger instance using `setup()`

```javascript
'use strict';

const logger = require('@da-fat-company/winston-wrapper').setup({
  // Global settings
  level: 'verbose',           // Default to 'info'
  handle_exceptions: false,   // Default to true
  exitOnError: false,         // Default to true
  
  // Console transport settings
  console: {
    enabled: false,     // Default to true
    options: {
      json: false,      // Default to true
      colorize: false,  // Default to true
      level: 'silly',   // You can specify a logging level only for this transport
      label: 'category one'
    }
  },

  // Loggly transport settings
  loggly: {
    enabled: true,    // Default to false
    options: {
      token: 'my-super-token',  // Required
      subdomain: 'foo',         // Required
      tags: [                   // Optional
        'Foo',
        'Bar',
        'Baz'
      ],
      json: false // Optional - Default to true
    }
  }
});

logger.log('Hello World');
```

Note: logger is just an instance of Winston.Logger so you can refer to the [Winston documentation](https://github.com/winstonjs/winston) for more examples.
