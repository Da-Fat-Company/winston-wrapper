'use strict';

const Logger = require('./lib/logger');
const loggerInstance = new Logger();

module.exports = loggerInstance.getLogger();
module.exports.setup = options => loggerInstance.configure(options).getLogger();
