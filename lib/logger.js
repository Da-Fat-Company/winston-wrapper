'use strict';

const Joi = require('joi');
const merge = require('merge');
const Winston = require('winston');
const LoggerSchema = require('./logger.schema');

require('winston-loggly');

class Logger {
  constructor(options) {
    this.options = {};

    // Transports list
    this.transports = {};

    this.configure(options);
  }

  /**
   * Configure and init the logger
   *
   * @param {Object?} options Custom option set
   * @return {Logger} This
   */
  configure(options) {
    this.options = merge.recursive(true, this.options, options||{});
    const validatedOptions = Joi.validate(this.options, LoggerSchema);

    if (validatedOptions.error) {
      throw new Error('Logger config validation error: ' + validatedOptions.error.message);
    }

    /****** Configure winston transports ******/
    // Console
    if (this.transports.console) {
      delete this.transports.console;
    }

    if (validatedOptions.value.console.enabled) {
      this.transports.console = new (Winston.transports.Console)(validatedOptions.value.console.options);
    }

    // Loggly
    if (this.transports.loggly) {
      delete this.transports.loggly;
    }

    if (validatedOptions.value.loggly.enabled) {
      this.transports.loggly = new (Winston.transports.Loggly)(validatedOptions.value.loggly.options);
    }

    /****** Build Winston options ******/
    const loggerOptions = {
      level: validatedOptions.value.level,
      transports: [],
      exceptionHandlers: [],
      exitOnError: validatedOptions.value.exitOnError,
    };

    // Add transports
    for (const key in this.transports) {
      loggerOptions.transports.push(this.transports[key]);
    }

    // Handle Exception using all transports
    if (validatedOptions.value.handle_exceptions) {
      loggerOptions.exceptionHandlers = loggerOptions.transports;
    }

    // Instantiate or configure Winston.Logger
    if (!this.logger) {
      this.logger = new Winston.Logger(loggerOptions);
    } else {
      this.logger.configure(loggerOptions);
    }

    return this;
  }

  /**
   * Get the Winston Logger instance
   *
   * @return {Winston.Logger} The Winston Logger instance
   */
  getLogger() {
    return this.logger;
  }
}

module.exports = Logger;
