'use strict';

const Joi = require('joi');
const merge = require('merge');

////////////////////////////////////
// Global Winston Options
////////////////////////////////////
const globalOptions = {
  level: Joi.string().allow([
    'error',
    'warn',
    'info',
    'verbose',
    'debug',
    'silly'
  ]).default('info'),
  handle_exceptions: Joi.boolean().truthy('TRUE').truthy('true').falsy('FALSE').falsy('false').default(true),
  exitOnError: Joi.boolean().truthy('TRUE').truthy('true').falsy('FALSE').falsy('false').default(true)
};

////////////////////////////////////
// Console Transport Options
////////////////////////////////////
const CONSOLE_DEFAULT_ENABLED = true;
const CONSOLE_DEFAULT_OPTIONS = {
  json: true,
  colorize: true
};
const CONSOLE_SCHEMA = Joi.object({
  enabled: Joi.boolean().truthy('TRUE').truthy('true').falsy('FALSE').falsy('false').default(CONSOLE_DEFAULT_ENABLED),
  options: Joi.object().default(CONSOLE_DEFAULT_OPTIONS).unknown()
}).default({
  enabled: CONSOLE_DEFAULT_ENABLED,
  options: CONSOLE_DEFAULT_OPTIONS
});

////////////////////////////////////
// Loggly Transport Options
////////////////////////////////////
const LOGGLY_DEFAULT_ENABLED = false;
const LOGGLY_OPTIONS_SCHEMA = {
  token: Joi.string().required(),
  subdomain: Joi.string().required(),
  tags: Joi.array().default([]),
  json: Joi.boolean().default(true)
};
const LOGGLY_SCHEMA = Joi.object({
  enabled: Joi.boolean().truthy('TRUE').truthy('true').falsy('FALSE').falsy('false').default(CONSOLE_DEFAULT_ENABLED),
  options: Joi.object(LOGGLY_OPTIONS_SCHEMA).unknown()
}).default({
  enabled: LOGGLY_DEFAULT_ENABLED
});

////////////////////////////////////
// Transport option list
////////////////////////////////////
const transportsOptions = {
  console: CONSOLE_SCHEMA,
  loggly: LOGGLY_SCHEMA
};

////////////////////////////////////
// Final merged schema
////////////////////////////////////
const schema = Joi.object(
  merge.recursive(false, globalOptions, transportsOptions)
).required();

module.exports = schema;